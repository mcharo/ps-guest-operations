﻿function Get-GuestProperty
{
    [CmdletBinding()]
    param(
        $VmName,
        $Property,
        $VboxManage = "$env:ProgramFiles\Oracle\VirtualBox\VBoxManage.exe",
        $VboxControl = "$env:SystemRoot\system32\VboxControl.exe"
    )
    if ($VmName)
    {
        if (Test-Path -Path $VboxManage)
        {
            $Value = & $VboxManage guestproperty get $VmName $Property
        }
    }
    else
    {
        if (Test-Path -Path $VboxControl)
        {
            $Value = & $VboxControl --nologo guestproperty get $Property
        }
    }
    $Value = $Value -replace 'Value\: '
    $Value.Trim()
}

function Add-GuestProperty
{
    [CmdletBinding()]
    param(
        $VmName,
        $Property,
        $Value,
        $VboxManage = "$env:ProgramFiles\Oracle\VirtualBox\VBoxManage.exe",
        $VboxControl = "$env:SystemRoot\system32\VboxControl.exe"
    )
    if ($VmName)
    {
        if (Test-Path -Path $VboxManage)
        {
            & $VboxManage guestproperty set $VmName $Property $Value
        }
    }
    else
    {
        if (Test-Path -Path $VboxControl)
        {
            & $VboxControl --nologo guestproperty set $Property $Value
        }
    }
}

New-Alias -Name Set-GuestProperty -Value Add-GuestProperty

function Get-KeyValuePair
{
    [CmdletBinding()]
    param(
        $VmName,
        $Property
    )
    if ($VmName)
    {
        $VmWmiObject = Get-WmiObject -Namespace root\virtualization\v2 -Class Msvm_ComputerSystem -Filter "ElementName='$VmName'"
        $VmWmiObject.Get()
        if ($VmWmiObject.EnabledState -eq 2)
        {
            $KvpRoot = $VmWmiObject.GetRelated('Msvm_KvpExchangeComponent')
            $KvpProperties = @()
            $KvpProperties += $KvpRoot.GetRelated('Msvm_KvpExchangeComponentSettingData').HostExchangeItems
            $KvpProperties += $KvpRoot.GuestExchangeItems
            $KvpProperties += $KvpRoot.GuestIntrinsicExchangeItems
            ForEach ($KvpProperty in $KvpProperties)
            {
                $KvpKey = (([xml]$KvpProperty).INSTANCE.PROPERTY | Where-Object { $_.Name -eq 'Name'}).VALUE
                Write-Verbose "Found $KvpKey"
                if ($KvpKey -eq $Property)
                {
                    return (([xml]$KvpProperty).INSTANCE.PROPERTY | Where-Object { $_.Name -eq 'Data'}).VALUE
                }
            }
        }
    }
    else
    {
        $RegistryKeys = @('Auto', 'External', 'Guest', 'Guest\Parameters')
        ForEach ($RegistryKey in $RegistryKeys)
        {
            $Value = Get-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Virtual Machine\$RegistryKey" -Name $Property -ErrorAction SilentlyContinue
            if ($Value)
            {
                return $Value.$Property
            }
        }
    }
}

function Add-KeyValuePair
{
    [CmdletBinding()]
    param(
        $VmName,
        $Property,
        $Value,
        [switch]$Force
    )
    if ($VmName)
    {
        $VmWmiObject = Get-WmiObject -Namespace root\virtualization\v2 -Class Msvm_ComputerSystem -Filter "ElementName='$VmName'"
        $VmMgmtService = Get-WmiObject -Namespace root\virtualization\v2 -Class Msvm_VirtualSystemManagementService
        $Kvp = ([wmiclass]"\\$($VmMgmtService.ClassPath.Server)\$($VmMgmtService.ClassPath.NamespacePath):Msvm_KvpExchangeDataItem").CreateInstance()
        $Kvp.Name = $Property
        $Kvp.Data = $Value
        $Kvp.Source = 0
        $KvpResult = $VmMgmtService.AddKvpItems($VmWmiObject, $Kvp.GetText(1))
        if ($Force)
        {
            $KvpResult = $VmMgmtService.ModifyKvpItems($VmWmiObject, $Kvp.GetText(1))
        }
    }
    else
    {
        New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Virtual Machine\External' -Name $Property -Value $Value -PropertyType String -Force:$Force
    }
}

function Set-KeyValuePair
{
    [CmdletBinding()]
    param(
        $VmName,
        $Property,
        $Value,
        [switch]$Force
    )
    if ($VmName)
    {
        $VmWmiObject = Get-WmiObject -Namespace root\virtualization\v2 -Class Msvm_ComputerSystem -Filter "ElementName='$VmName'"
        $VmMgmtService = Get-WmiObject -Namespace root\virtualization\v2 -Class Msvm_VirtualSystemManagementService
        $Kvp = ([wmiclass]"\\$($VmMgmtService.ClassPath.Server)\$($VmMgmtService.ClassPath.NamespacePath):Msvm_KvpExchangeDataItem").CreateInstance()
        $Kvp.Name = $Property
        $Kvp.Data = $Value
        $Kvp.Source = 0
        if ($Force)
        {
            $KvpResult = $VmMgmtService.AddKvpItems($VmWmiObject, $Kvp.GetText(1))
        }
        $KvpResult = $VmMgmtService.ModifyKvpItems($VmWmiObject, $Kvp.GetText(1))
    }
    else
    {
        Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Virtual Machine\External' -Name $Property -Value $Value -PropertyType String -Force:$Force
    }
}